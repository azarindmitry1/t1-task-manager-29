package ru.t1.azarin.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}